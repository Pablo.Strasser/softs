#!/bin/sh

#SBATCH --job-name="test fluent"
#SBATCH --ntasks=32
#SBATCH --time=15:00
#SBATCH --partition=debug
#SBATCH --output=slurm-%J.out

module load foss/2016a
module load fluent
module load python/279

# Generate a list of nodes from the SLURM environment
# https://www.nsc.liu.se/~kent/python-hostlist/
python /opt/hostlist/hostlist --expand --append=':' --append-slurm-tasks=$SLURM_TASKS_PER_NODE \
     $SLURM_JOB_NODELIST > machinefile

MYINPUTFILE=./execution-script


fluent 3d -mpi=openmpi -pib -g -cnf=machinefile -i $MYINPUTFILE
