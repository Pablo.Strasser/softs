#!/bin/bash

#SBATCH --partition=mono-shared
#SBATCH --time=12:00:00
#SBATCH --cpus-per-task=12
#SBATCH --output=slurm-%J.out

module load FreeSurfer/6.0.0-centos6_x86_64

export FREESURFER_HOME=$EBROOTFREESURFER
source $FREESURFER_HOME/SetUpFreeSurfer.sh
export SUBJECTS_DIR=/subjects_paths

srun recon-all -subject xxx -i xxx -autorecon-all -qcache -notal-check -parallel -openmp 12
