#!/bin/sh


#SBATCH --cpus-per-task=1
#SBATCH --job-name=testTensorFlow
#SBATCH --ntasks=1
#SBATCH --time=05:00
#SBATCH --output=slurm-%J.out
#SBATCH --gres=gpu:1
#SBATCH --constraint="V5|V6"
#SBATCH --partition=shared-gpu-EL7


## TensorFlow
module load fosscuda/2018b TensorFlow/1.10.1-Python-2.7.15

srun python helloworld.py
