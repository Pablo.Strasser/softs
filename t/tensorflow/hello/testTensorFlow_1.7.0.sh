#!/bin/sh


#SBATCH --cpus-per-task=1
#SBATCH --job-name=testTensorFlow
#SBATCH --ntasks=1
#SBATCH --time=05:00
#SBATCH --output=slurm-%J.out
#SBATCH --gres=gpu:1
#SBATCH --constraint="V5|V6"
#SBATCH --partition=shared-gpu-EL7


## TensorFlow
module load GCC/6.4.0-2.28 OpenMPI/2.1.2 TensorFlow/1.7.0-Python-3.6.4
## CUDA
module load cuDNN/7.0.5-CUDA-9.1.85


srun python helloworld.py
