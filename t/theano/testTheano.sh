#!/bin/sh 
#SBATCH --cpus-per-task=1
#SBATCH --job-name=testTheano
#SBATCH --ntasks=1
#SBATCH --time=05:00
#SBATCH --output=slurm-%J.out
#SBATCH --gres=gpu:1
#SBATCH --partition=shared-gpu

module load foss/2018a Theano/1.0.1-Python-3.6.4 libgpuarray/0.7.5 cuDNN/7.0.5-CUDA-9.1.85

#THEANO_FLAGS=device=cpu
export THEANO_FLAGS=device=cuda

export DEVICE=cuda

#srun python -c "import pygpu;pygpu.test()"

#workaround for this issue: https://github.com/Theano/Theano/issues/5648
export OMPI_MCA_mpi_warn_on_fork=0

srun python testTheanoDevice.py
