import bpy


prefs = bpy.context.user_preferences.addons['cycles'].preferences
print(prefs.compute_device_type)

for d in prefs.devices:
    print(d.name)

bpy.context.user_preferences.addons['cycles'].preferences.compute_device_type = 'CUDA'
bpy.context.user_preferences.addons['cycles'].preferences.devices[0].use = True
