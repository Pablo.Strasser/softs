#!/bin/bash
#SBATCH --job-name=testR
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --time=0:15:0
#SBATCH --partition=debug

module load foss/2016b R/3.4.2

INFILE=hello.R
OUTFILE=hello.Rout

srun R CMD BATCH $INFILE $OUTFILE

