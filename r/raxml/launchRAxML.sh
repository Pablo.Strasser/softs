#!/bin/sh

#SBATCH --cpus-per-task=1
#SBATCH --job-name=run2_raxml
#SBATCH --ntasks=16
#SBATCH --partition=debug
#SBATCH --output=slurm-%J.out

module load foss/2016a RAxML/8.2.4-hybrid-avx

srun raxmlHPC-HYBRID-AVX \
-s *.phy \
-m GTRGAMMAI \
-q *.txt \
-p 5413641783648917 -c 4 -f d -i 10 -T 16 \
-# 50 -n noHypPeck
