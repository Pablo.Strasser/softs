#!/bin/sh


## Slurm
#SBATCH --job-name=SAT
#SBATCH --exclusive
#SBATCH --mem=0
#SBATCH --nodes=1
#SBATCH --output="%N___slurm-%j.out"


## functions
get_CurrentWatts() {
    scontrol -o show Node="${NODE}" | \
     sed -e 's/^.*CurrentWatts=\([0-9]*\) LowestJoules=.*$/\1/'
}


## checks
if [ "x$1" = x-h ]; then
    cat <<EOF
Usage: $0 [stressapptest options]
EOF
    exit 1
fi


## vars
NODE=$(hostname -s)
CURRENTWATTS_IDLE=$(get_CurrentWatts)
CURRENTWATTS_HIGHEST=${CURRENTWATTS_IDLE}
SLEEP_TIME=$(expr \
              $(grep -e 'JobAcctGatherFrequency' /etc/slurm/slurm.conf | \
                 sed -e 's/^.*energy=//g') \
              + \
              $(awk -F"=" '/^EnergyIPMIFrequency/ {print $2}' /etc/slurm/acct_gather.conf))


## main
cat <<EOF
W: waiting ${SLEEP_TIME} seconds before getting the idle value..."
   (this correspond to the sum of energy values in:
     - /etc/slurm/slurm.conf:JobAcctGatherFrequency
     - /etc/slurm/acct_gather.conf:EnergyIPMIFrequency)
EOF
sleep ${SLEEP_TIME}

echo "I: idle: ${CURRENTWATTS_IDLE}"
echo "I: running..."
./stressapptest_generic.sh "$@" |
 while read LINE; do
    SECONDS_REMAINING=$(echo "${LINE}" | 
                         awk '/^Log: Seconds remaining: / {print $4}')
    if [ -n "${SECONDS_REMAINING}" ]; then
         CURRENTWATTS_NOW=$(get_CurrentWatts)
	 [ ${CURRENTWATTS_HIGHEST} -lt ${CURRENTWATTS_NOW} ] && \
          CURRENTWATTS_HIGHEST=${CURRENTWATTS_NOW}
    fi
 done

echo "W: waiting $(expr ${SLEEP_TIME} / 2) seconds before getting the maximum value..."
sleep $(expr ${SLEEP_TIME} / 2)
CURRENTWATTS_FINAL=$(get_CurrentWatts)
[ ${CURRENTWATTS_HIGHEST} -lt ${CURRENTWATTS_FINAL} ] && \
 CURRENTWATTS_HIGHEST=${CURRENTWATTS_FINAL}
echo "I: highest: ${CURRENTWATTS_HIGHEST}"
