#!/bin/sh

#SBATCH --time=00:15:00
#SBATCH --ntasks=1
#SBATCH --mem=1G
#SBATCH -J hello-stata
#SBATCH -e slurm-%j.err
#SBATCH -o slurm-%j.out
#SBATCH --cpus-per-task=2
#SBATCH --partition=debug

module load stata/14mp24

srun stata-mp hello.do
