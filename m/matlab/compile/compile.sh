#!/bin/sh

# This script takes a matlab file (.m) as input and produce a binary of the same basename


fullfile=$1

filename="${fullfile##*/}"
extension="${filename##*.}"
filenamebase="${filename%.*}"

# we check the file extension
if [ 'x'$extension != 'xm' ]
then
   echo "Bad filextension. Please specify a .m file"
   exit
fi

echo "Compiling $filename, output will be $filenamebase"

module load foss/2016a
module load matlab/2016a

DISPLAY="" mcc -m -v -R '-nojvm, -nodisplay' -o $filenamebase $filename

