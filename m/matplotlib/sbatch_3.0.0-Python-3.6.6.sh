#!/bin/bash


## deps
module load GCC/7.3.0-2.30  OpenMPI/3.1.1

module load matplotlib/3.0.0-Python-3.6.6


## main
srun python example.py
